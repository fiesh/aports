# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=mpg123
pkgver=1.29.1
pkgrel=0
pkgdesc="Console-based MPEG Audio Player for Layers 1, 2 and 3"
options="libtool"
url="https://www.mpg123.org"
arch="all"
license="LGPL-2.1-only"
subpackages="$pkgname-libs $pkgname-dev $pkgname-doc"
makedepends="libtool alsa-lib-dev linux-headers"
source="https://www.mpg123.org/download/mpg123-$pkgver.tar.bz2"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--disable-dependency-tracking \
		--with-pic \
		--with-optimization=0 \
		--with-cpu=i386_fpu \
		--with-audio="alsa oss"
	make
}

check() {
	make check
}

package() {
	# Installation is not parallel friendly and will fail
	# sometimes
	make -j1 DESTDIR="$pkgdir" install
}

sha512sums="
67d1d122fa41079fd7d4e2ced4bb072178adf646833c7a2aaf8f32414dbf378dda94aa536e3bba396e1e61351078a3217189fb176fca4714b4dc786404eaffc9  mpg123-1.29.1.tar.bz2
"
