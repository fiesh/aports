# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plasma-thunderbolt
pkgver=5.23.1
pkgrel=0
# armhf blocked by qt5-qtdeclarative
# s390x, mips64 and riscv64 blocked by bolt -> polkit
arch="all !armhf !s390x !mips64 !riscv64"
url="https://kde.org/plasma-desktop/"
pkgdesc="Plasma integration for controlling Thunderbolt devices"
license="GPL-2.0-only OR GPL-3.0-only"
depends="bolt"
makedepends="
	extra-cmake-modules
	kcmutils-dev
	kcoreaddons-dev
	kdbusaddons-dev
	kdeclarative-dev
	ki18n-dev
	knotifications-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	"
checkdepends="xvfb-run"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/plasma-thunderbolt-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # Requires running dbus server

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
3a478c5af64b01cd5595d8b0497194b9862e4017d9e4fd2d0c5941aeb8179b656a6adbc8d09490e0c5968dc952e347ebe6137a83eb1e50b42abe4566cc63ba46  plasma-thunderbolt-5.23.1.tar.xz
"
