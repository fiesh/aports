# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plasma-nano
pkgver=5.23.1
pkgrel=0
pkgdesc="A minimal Plasma shell package intended for embedded devices"
# armhf blocked by extra-cmake-modules
# s390x, mips64 and riscv64 blocked by polkit -> plasma-framework
arch="all !armhf !s390x !mips64 !riscv64"
url="https://invent.kde.org/kde/plasma-nano"
license="GPL-2.0-or-later AND LGPL-2.1-or-later"
depends="qt5-qtvirtualkeyboard"
makedepends="
	extra-cmake-modules
	kwayland-dev
	kwindowsystem-dev
	plasma-framework-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/plasma-nano-$pkgver.tar.xz"
options="!check" # No tests

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
b70c07052189ebf5cbf6b8fbf832d995ffd8c94bc14d8143b5bfa0487b3e147f756839a826d109497cd50f76abb62784ecc79cb32dcc8c3834b140cf5abbaf5e  plasma-nano-5.23.1.tar.xz
"
